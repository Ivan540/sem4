#include "widget.h"

Widget::Widget(QWidget *parent)
    : QFrame(parent)
{
        setupUi(this);
        depthValueMax=10;
        deltaDepth=0.5;
        connect (btnGlubinaPlus,SIGNAL(clicked()),
                 this, SLOT(addDepthValue()));

}

Widget::~Widget()
{

}

void Widget::on_btnGlubinaPlus_clicked()
{

valueDepth += deltaDepth;
if (valueDepth > depthValueMax)
valueDepth = depthValueMax;
btnGlubinaReset->setText(QString::number(valueDepth));

}
void Widget::on_btnGlubinaMinus_clicked()
{

valueDepth -= deltaDepth;
if (valueDepth < 0)
valueDepth = 0;
btnGlubinaReset->setText(QString::number(valueDepth));

}
void Widget::on_btnGlubinaReset_clicked()
{

valueDepth = 0;

btnGlubinaReset->setText(QString::number(valueDepth));

}

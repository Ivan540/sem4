#ifndef WIDGET_H
#define WIDGET_H
#include "ui_signalSlotChallenge.h"
#include <QFrame>

class Widget : public QFrame, public Ui::ControlFrame
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();
    double  depthValueMax;
    double deltaDepth;
    double valueDepth;
signals:
    void DepthValueChanged();
public slots:
    void on_btnGlubinaPlus_clicked();
    void on_btnGlubinaMinus_clicked();
    void on_btnGlubinaReset_clicked();
};


#endif // WIDGET_H
